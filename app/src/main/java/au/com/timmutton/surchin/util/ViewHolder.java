package au.com.timmutton.surchin.util;

import android.util.SparseArray;
import android.view.View;

/***
 * Generic view holder to hold whatever we want inside the tag of the view
 * fetchable via ID
 */
public class ViewHolder {
    // generic return type to reduce the casting noise in client code
    public static <T> T get(View view, int id) {
        SparseArray<Object> viewHolder = getHolderArray(view);
        Object childView = viewHolder.get(id);

        if (childView == null) {
            childView = view.findViewById(id);
            viewHolder.put(id, childView);
        }

        return (T) childView;
    }

    private static SparseArray<Object> getHolderArray(View view) {
        SparseArray<Object> viewHolder = (SparseArray<Object>) view.getTag();

        if (viewHolder == null) {
            viewHolder = new SparseArray<>();
            view.setTag(viewHolder);
        }

        return viewHolder;
    }
}