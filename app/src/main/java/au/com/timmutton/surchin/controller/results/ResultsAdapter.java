package au.com.timmutton.surchin.controller.results;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import au.com.timmutton.surchin.R;
import au.com.timmutton.surchin.util.ViewHolder;

/**
 * Created by tim on 13/01/15.
 */
class ResultsAdapter extends ArrayAdapter<String> {
    private final LayoutInflater mInflater;
    private final Context mContext;

    public ResultsAdapter(Context context) {
        super(context, 0);

        mInflater = LayoutInflater.from(context);
        mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String result = getItem(position);

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.result_grid_item, parent, false);
        }

        final ProgressBar spinner = ViewHolder.get(convertView, R.id.grid_progress);
        final ImageView imageView = ViewHolder.get(convertView, R.id.grid_image);

        Picasso.with(mContext).load(result).into(imageView, new Callback() {
            @Override
            public void onSuccess() {
                spinner.setVisibility(View.GONE);
            }

            @Override
            public void onError() {
                spinner.setVisibility(View.GONE);
            }
        });

        return convertView;
    }
}
