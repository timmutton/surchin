package au.com.timmutton.surchin.controller.home;

import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;

import au.com.timmutton.surchin.R;
import butterknife.Bind;

import static butterknife.ButterKnife.bind;

public class HomeActivity extends AppCompatActivity {
    @Bind(R.id.body_search)
    SearchView mSearchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home);
        bind(this);

        // Get the searchable info
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchableInfo searchableInfo = searchManager.getSearchableInfo(getComponentName());
        // Set up search view
        mSearchView.setSearchableInfo(searchableInfo);
        mSearchView.setQueryRefinementEnabled(true);
    }
}
