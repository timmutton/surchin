package au.com.timmutton.surchin.application;

import android.app.Application;

import au.com.timmutton.surchin.BuildConfig;
import timber.log.Timber;

/**
 * Created by tim on 13/01/15.
 * Initialises Universal Image Loader so that it can be used for the results
 */
public class SurchinApplication extends Application {
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }
}
