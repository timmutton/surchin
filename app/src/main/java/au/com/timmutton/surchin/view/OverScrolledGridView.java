package au.com.timmutton.surchin.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.GridView;

/**
 * Created by tim on 14/01/15.
 */
public class OverScrolledGridView extends GridView {
    private OnOverScrolledListener mListener = null;

    public OverScrolledGridView(Context context) {
        super(context);
    }

    public OverScrolledGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public OverScrolledGridView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(21)
    public OverScrolledGridView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected boolean overScrollBy(int deltaX, int deltaY, int scrollX, int scrollY, int scrollRangeX, int scrollRangeY, int maxOverScrollX, int maxOverScrollY, boolean isTouchEvent) {
        if(mListener != null) {
            mListener.onOverScroll(deltaY);
        }

        return super.overScrollBy(deltaX, deltaY, scrollX, scrollY, scrollRangeX, scrollRangeY, maxOverScrollX, maxOverScrollY, isTouchEvent);
    }

    public void setOverScrolledListener(OnOverScrolledListener listener) {
        mListener = listener;
    }

    public interface OnOverScrolledListener {
        void onOverScroll(int deltaY);
    }
}
