package au.com.timmutton.surchin.util;

import android.content.SearchRecentSuggestionsProvider;

/**
 * Created by tim on 13/01/15.
 */
public class HistorySuggestionProvider extends SearchRecentSuggestionsProvider {
    public final static String AUTHORITY = "au.com.timmutton.surchin.util.HistorySuggestionProvider";
    public final static int MODE = DATABASE_MODE_QUERIES;

    public HistorySuggestionProvider() {
        setupSuggestions(AUTHORITY, MODE);
    }
}
