package au.com.timmutton.surchin.controller.results;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.SearchRecentSuggestions;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import au.com.timmutton.surchin.R;
import au.com.timmutton.surchin.model.ImageResult;
import au.com.timmutton.surchin.model.SearchResult;
import au.com.timmutton.surchin.util.HistorySuggestionProvider;
import au.com.timmutton.surchin.view.OverScrolledGridView;
import butterknife.Bind;
import timber.log.Timber;

import static butterknife.ButterKnife.bind;

/**
 * Created by tim on 13/01/15.
 */
public class ResultsActivity extends AppCompatActivity implements OverScrolledGridView.OnOverScrolledListener {
    private static final int MAX_QUERIES = 64; // Limit set by google since api is deprecated
    private static final int GRID_COLUMNS = 3;
    private static final int RESULT_SIZE = 6; // Must be an integer between 1 and 8. 6 is chosen because it perfectly fills 2 rows
    private static final int ROWS_PER_QUERY = RESULT_SIZE / GRID_COLUMNS;
    private static final String SEARCH_ENDPOINT = "https://ajax.googleapis.com/ajax/services/search/images?v=1.0&rsz=" + String.valueOf(RESULT_SIZE);

    @Bind(R.id.result_grid)
    OverScrolledGridView mGridView;
    @Bind(R.id.result_progress)
    ProgressBar mProgressBar;

    private ResultsAdapter mAdapter;
    private int mQueriesPerLoad, mAsyncTaskCount = 0;
    private String mCurrentQuery;
    private Toast mToast;

    private final OkHttpClient client = new OkHttpClient();
    private final Gson gson = new Gson();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_results);
        bind(this);

        // Using a reusable toast so that I can check visibility and not worry about context
        mToast = Toast.makeText(this, "", Toast.LENGTH_SHORT);

        mAdapter = new ResultsAdapter(this);
        mGridView.setAdapter(mAdapter);
        // Custom overscrolled listener is used instead of standard scroll listener so that I
        // can re-warn the user they've reached the bottom of the list if they scroll
        // whilst already at the bottom
        mGridView.setOverScrolledListener(this);

        // Get the screen width divided by number of columns to work out pixel size per column
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        float gridItemWidth = (float)(size.x / GRID_COLUMNS);

        // Since images are square, the amount of rows that will fit on screen is the grid width divided by screen height
        // We device this by how many rows each query will will fill to work out how many queries we will need to do to fill a page
        mQueriesPerLoad = (int)Math.ceil(size.y / gridItemWidth / ROWS_PER_QUERY);

        handleIntent(getIntent());
    }

    @Override
    public void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        return super.onPrepareOptionsMenu(menu);
    }

    private void handleIntent(Intent intent) {
        if (intent != null && Intent.ACTION_SEARCH.equals(intent.getAction())) {
            mCurrentQuery = intent.getStringExtra(SearchManager.QUERY);

            SearchRecentSuggestions suggestions = new SearchRecentSuggestions(this,
                    HistorySuggestionProvider.AUTHORITY, HistorySuggestionProvider.MODE);
            suggestions.saveRecentQuery(mCurrentQuery, null);

            // Reset the adapter and start loading from first index
            mAdapter.clear();
            loadPage(0);
        }
    }

    private void loadPage(int start) {
        mAsyncTaskCount = mQueriesPerLoad;

        if((start + RESULT_SIZE) >= MAX_QUERIES) {
            mProgressBar.setVisibility(View.GONE);

            if(!mToast.getView().isShown()) {
                mToast.setText(R.string.results_end_of_list);
                mToast.show();
            }
        } else {
            mProgressBar.setVisibility(View.VISIBLE);
        }

        for(int i = 0; i < mQueriesPerLoad; ++i) {
            int startOffset = start + i * RESULT_SIZE;
            if((startOffset + RESULT_SIZE) > MAX_QUERIES) {
                mAsyncTaskCount -= (mQueriesPerLoad - i);
                break;
            } else {
                new AsyncImageLoader().execute(String.valueOf(startOffset));
            }
        }
    }

    @Override
    public void onOverScroll(int deltaY) {
        if(deltaY > 0 && (mAsyncTaskCount == 0)) {
            loadPage(mAdapter.getCount());
        }
    }

    // Use AsyncTask ao that we can load a couple at a time as well as not block io while loading.
    // Actual order of results shouldn't matter
    private class AsyncImageLoader extends AsyncTask<String, Void, Response> {
        @Override
        protected Response doInBackground(String... params) {
            Response response = null;
            String startOffset = params[0];

            String encodedUrl;
            try {
                encodedUrl = URLEncoder.encode(mCurrentQuery, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                Timber.e(e.getMessage());
                encodedUrl = mCurrentQuery;
            }

            Request request = new Request.Builder()
                    .url(SEARCH_ENDPOINT + "&q=" + encodedUrl + "&start=" + startOffset)
                    .build();

            try {
                response = client.newCall(request).execute();
            } catch (IOException e) {
                Timber.e(e.getMessage());
            }

            return response;
        }

        @Override
        protected void onPostExecute(Response response) {
            super.onPostExecute(response);

            if (response.isSuccessful()) {
                try {
                    SearchResult result = gson.fromJson(response.body().charStream(), SearchResult.class);
                    for (ImageResult imageResult : result.responseData.results) {
                        mAdapter.add(imageResult.tbUrl);
                    }
                } catch (IOException e) {
                    Timber.e(e.getMessage());
                }
            } else {
                mToast.setText(R.string.results_unable_to_access);
                mToast.show();
            }

            if (--mAsyncTaskCount == 0) {
                mProgressBar.setVisibility(View.GONE);
            }
        }
    }
}
